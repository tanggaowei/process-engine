package com.yongdui.process;

import com.yongdui.process.operator.OperatorOfApply;
import com.yongdui.process.operator.OperatorOfApproval;
import com.yongdui.process.operator.OperatorOfNotify;
import com.yongdui.process.operator.OperatorOfSimpleGateway;
import junit.framework.TestCase;

public class ProcessEngineTest extends TestCase {

    /**
     * 条件分支的测试
     */
    public void testTestRun3() throws InterruptedException {
        String xmlstr = "<definitions>\n" +
                "    <process id=\"process_2\" name=\"简单审批例子\">\n" +
                "        <startEvent id=\"startEvent_1\">\n" +
                "            <outgoing>flow_1</outgoing>\n" +
                "        </startEvent>\n" +
                "        <sequenceFlow id=\"flow_1\" sourceRef=\"startEvent_1\" targetRef=\"approvalApply_1\"/>\n" +
                "        <approvalApply id=\"approvalApply_1\" name=\"提交申请单\">\n" +
                "            <incoming>flow_1</incoming>\n" +
                "            <incoming>flow_5</incoming>\n" +
                "            <outgoing>flow_2</outgoing>\n" +
                "        </approvalApply>\n" +
                "        <sequenceFlow id=\"flow_2\" sourceRef=\"approvalApply_1\" targetRef=\"approval_1\"/>\n" +
                "        <approval id=\"approval_1\" name=\"审批\">\n" +
                "            <incoming>flow_2</incoming>\n" +
                "            <outgoing>flow_3</outgoing>\n" +
                "        </approval>\n" +
                "        <sequenceFlow id=\"flow_3\" sourceRef=\"approval_1\" targetRef=\"simpleGateway_1\"/>\n" +
                "        <simpleGateway id=\"simpleGateway_1\" name=\"简单是非判断\">\n" +
                "            <trueOutGoing>flow_4</trueOutGoing>\n" +
                "            <expr>approvalResult</expr>\n" +
                "            <incoming>flow_3</incoming>\n" +
                "            <outgoing>flow_4</outgoing>\n" +
                "            <outgoing>flow_5</outgoing>\n" +
                "        </simpleGateway>\n" +
                "        <sequenceFlow id=\"flow_5\" sourceRef=\"simpleGateway_1\" targetRef=\"approvalApply_1\"/>\n" +
                "        <sequenceFlow id=\"flow_4\" sourceRef=\"simpleGateway_1\" targetRef=\"notify_1\"/>\n" +
                "        <notify id=\"notify_1\" name=\"结果邮件通知\">\n" +
                "            <incoming>flow_4</incoming>\n" +
                "            <outgoing>flow_6</outgoing>\n" +
                "        </notify>\n" +
                "        <sequenceFlow id=\"flow_6\" sourceRef=\"notify_1\" targetRef=\"endEvent_1\"/>\n" +
                "        <endEvent id=\"endEvent_1\">\n" +
                "            <incoming>flow_6</incoming>\n" +
                "        </endEvent>\n" +
                "    </process>\n" +
                "</definitions>";
        ProcessEngine processEngine = new ProcessEngine(xmlstr);

        processEngine.registNodeProcessor(new OperatorOfApproval());
        processEngine.registNodeProcessor(new OperatorOfApply());
        processEngine.registNodeProcessor(new OperatorOfNotify());
        processEngine.registNodeProcessor(new OperatorOfSimpleGateway());

        processEngine.start();

        // 等待审批流程执行完成，否则可能导致审批流程来不及执行，程序就退出了
        Thread.sleep(1000 * 1);

        /* 打印结果
        approvalResult ： false，price : 400
        approvalResult ： false，price : 300
        approvalResult ： true，price : 200
        小张提交的200，被经理审批，结果为：true
        process finished!
         */
    }

    /**
     * 简单的审批
     */
    public void testTestRun2() throws InterruptedException {
        String xmlstr = "<definitions>\n" +
                "    <process id=\"process_2\" name=\"简单审批例子\">\n" +
                "        <startEvent id=\"startEvent_1\">\n" +
                "            <outgoing>flow_1</outgoing>\n" +
                "        </startEvent>\n" +
                "        <sequenceFlow id=\"flow_1\" sourceRef=\"startEvent_1\" targetRef=\"approvalApply_1\" />\n" +
                "        <approvalApply id=\"approvalApply_1\" name=\"提交申请单\">\n" +
                "            <incoming>flow_1</incoming>\n" +
                "            <outgoing>flow_2</outgoing>\n" +
                "        </approvalApply>\n" +
                "        <sequenceFlow id=\"flow_2\" sourceRef=\"approvalApply_1\" targetRef=\"approval_1\" />\n" +
                "        <approval id=\"approval_1\" name=\"审批\">\n" +
                "            <incoming>flow_2</incoming>\n" +
                "            <outgoing>flow_3</outgoing>\n" +
                "        </approval>\n" +
                "        <sequenceFlow id=\"flow_3\" sourceRef=\"approval_1\" targetRef=\"notify_1\"/>\n" +
                "        <notify id=\"notify_1\" name=\"结果邮件通知\">\n" +
                "            <incoming>flow_3</incoming>\n" +
                "            <outgoing>flow_4</outgoing>\n" +
                "        </notify>\n" +
                "        <sequenceFlow id=\"flow_4\" sourceRef=\"notify_1\" targetRef=\"endEvent_1\"/>\n" +
                "        <endEvent id=\"endEvent_1\">\n" +
                "            <incoming>flow_4</incoming>\n" +
                "        </endEvent>\n" +
                "    </process>\n" +
                "</definitions>";
        ProcessEngine processEngine = new ProcessEngine(xmlstr);

        processEngine.registNodeProcessor(new OperatorOfApply());
        processEngine.registNodeProcessor(new OperatorOfApproval());
        processEngine.registNodeProcessor(new OperatorOfNotify());

        processEngine.start();

        // 等待审批流程执行完成，否则可能导致审批流程来不及执行，程序就退出了
        Thread.sleep(1000);

        /** 打印结果
         approvalResult ： false，price : 400
         小张提交的400，被经理审批，结果为：false
         process finished!
         */
    }

    /**
     * Hello World（由于代码已升级，因此以下测试无法通过）
     */
//    public void testTestRun() {
//        String xmlstr = "<definitions>\n" +
//                "    <process id=\"process_1\" name=\"hello\">\n" +
//                "        <startEvent id=\"startEvent_1\">\n" +
//                "            <outgoing>flow_1</outgoing>\n" +
//                "        </startEvent>\n" +
//                "        <sequenceFlow id=\"flow_1\" sourceRef=\"startEvent_1\" targetRef=\"printHello_1\" />\n" +
//                "        <printHello id=\"printHello_1\" name=\"hello\">\n" +
//                "            <incoming>flow_1</incoming>\n" +
//                "            <outgoing>flow_2</outgoing>\n" +
//                "        </printHello>\n" +
//                "        <sequenceFlow id=\"flow_2\" sourceRef=\"printHello_1\" targetRef=\"printProcessEngine_1\" />\n" +
//                "        <printProcessEngine id=\"printProcessEngine_1\" name=\"processEngine\">\n" +
//                "            <incoming>flow_2</incoming>\n" +
//                "            <outgoing>flow_3</outgoing>\n" +
//                "        </printProcessEngine>\n" +
//                "        <sequenceFlow id=\"flow_3\" sourceRef=\"printProcessEngine_1\" targetRef=\"endEvent_1\"/>\n" +
//                "        <endEvent id=\"endEvent_1\">\n" +
//                "            <incoming>flow_3</incoming>\n" +
//                "        </endEvent>\n" +
//                "    </process>\n" +
//                "</definitions>";
//        ProcessEngine processEngine = new ProcessEngine(xmlstr);
//        processEngine.run();
//    }
}