package com.yongdui.process.operator;

import com.yongdui.process.ProcessEngine;
import com.yongdui.process.model.PeContext;
import com.yongdui.process.model.PeNode;

public class OperatorOfNotify implements IOperator {
    @Override
    public String getType() {
        return "notify";
    }

    @Override
    public void doTask(ProcessEngine processEngine, PeNode node, PeContext peContext) {

        System.out.println(String.format("%s提交的%s，被%s审批，结果为：%s",
                peContext.getValue("applicant"),
                peContext.getValue("price"),
                peContext.getValue("approver"),
                peContext.getValue("approvalResult")));

        processEngine.nodeFinished(node.onlyOneOut());
    }
}
