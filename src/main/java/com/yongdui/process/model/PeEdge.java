package com.yongdui.process.model;

/**
 * 边
 */
public class PeEdge {
    public String id;
    public PeNode from;
    public PeNode to;

    public PeEdge(String id) {
        this.id = id;
    }
}
