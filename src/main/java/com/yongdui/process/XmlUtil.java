package com.yongdui.process;

import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class XmlUtil {
    public static Node strToNode(String xmlstr) {
        Document document = parseXMLDocument(xmlstr);
        Element element = document.getDocumentElement();
        return element;
    }

    public static Node childByName(Node node, String name) {
        NodeList childNodes = node.getChildNodes();
        int length = childNodes.getLength();
        for (int i = 0; i < length; i++) {
            Node item = childNodes.item(i);
            if (name.equalsIgnoreCase(item.getNodeName())) {
                return item;
            }
        }
        return null;
    }

    public static List<Node> childsByName(Node node, String name) {
        List<Node> restul = new ArrayList<>();

        NodeList childNodes = node.getChildNodes();
        int length = childNodes.getLength();
        for (int i = 0; i < length; i++) {
            Node item = childNodes.item(i);
            if (name.equalsIgnoreCase(item.getNodeName())) {
                restul.add(item);
            }
        }
        return restul;
    }

    public static String attributeValue(Node node, String name) {
        NamedNodeMap nodeMap = node.getAttributes();
        for (int i = 0; i < nodeMap.getLength(); i++) {
            String nodeName = nodeMap.item(i).getNodeName();
            if (name.equalsIgnoreCase(nodeName)) {
                return nodeMap.item(i).getNodeValue();
            }
        }

        return null;
    }

    public static String text(Node node) {
        String nodeValue = node.getFirstChild().getNodeValue();
        return nodeValue;
    }

    public static String childTextByName(Node node, String name) {
        Node child = childByName(node, name);
        return text(child);
    }


    /**
     * 初始化一个空Document对象返回。
     *
     * @return a Document
     */
    public static Document newXMLDocument() {
        try {
            return newDocumentBuilder().newDocument();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 初始化一个DocumentBuilder
     */
    public static DocumentBuilder newDocumentBuilder() throws ParserConfigurationException {
        return newDocumentBuilderFactory().newDocumentBuilder();
    }

    /**
     * 初始化一个DocumentBuilderFactory
     *
     * @return a DocumentBuilderFactory
     */
    public static DocumentBuilderFactory newDocumentBuilderFactory() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        return dbf;
    }

    /**
     * 将传入的一个XML String转换成一个org.w3c.dom.Document对象返回。
     *
     * @param xmlString 一个符合XML规范的字符串表达。
     * @return a Document
     */
    public static Document parseXMLDocument(String xmlString) {
        if (xmlString == null) {
            throw new IllegalArgumentException();
        }
        try {
            return newDocumentBuilder().parse(new InputSource(new StringReader(xmlString)));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
