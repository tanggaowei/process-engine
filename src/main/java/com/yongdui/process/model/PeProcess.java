package com.yongdui.process.model;

/**
 * 流程
 * 说明：一个流程(PeProcess)由多个节点(PeNode)与边(PeEdge)组成，节点有出边(out)、入边(in)，边有流入节点(from)、流出节点(to)
 * 流程引擎的运行逻辑如下：
 * 1、找到开始节点(startEvent)
 * 2、找到 startEvent 的 outgoing 边(sequenceFlow)
 * 3、找到该边(sequenceFlow)指向的节点(targetRef)
 * 4、执行节点自身的逻辑
 * 5、找到该节点的outgoing边(sequenceFlow)
 * 6、重复3-5，直到遇到结束节点(endEvent)，流程结束
 * https://www.cnblogs.com/glittering-prizes/p/15434936.html
 */
public class PeProcess {
    public String id;
    public PeNode start;

    public PeProcess(String id, PeNode start) {
        this.id = id;
        this.start = start;
    }
}
