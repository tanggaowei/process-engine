package com.yongdui.process.operator;

import com.yongdui.process.ProcessEngine;
import com.yongdui.process.model.PeContext;
import com.yongdui.process.model.PeNode;

public interface IOperator {
    //引擎可以据此来找到本算子
    String getType();

    //引擎调度本算子
    void doTask(ProcessEngine processEngine, PeNode node, PeContext peContext);
}
