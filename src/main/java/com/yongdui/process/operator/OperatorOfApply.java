package com.yongdui.process.operator;

import com.yongdui.process.ProcessEngine;
import com.yongdui.process.model.PeContext;
import com.yongdui.process.model.PeNode;

public class OperatorOfApply implements IOperator {
    public static int price = 500;

    @Override
    public String getType() {
        return "approvalApply";
    }

    @Override
    public void doTask(ProcessEngine processEngine, PeNode node, PeContext peContext) {
        //price每次减100
        peContext.putValue("price", price -= 100);
        peContext.putValue("applicant", "小张");

        processEngine.nodeFinished(node.onlyOneOut());
    }
}